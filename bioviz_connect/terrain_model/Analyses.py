class Analyses:
    def __init__(self):
        self.description = ''
        self.name = ""
        self.can_share = False
        self.username = ""
        self.app_id = ""
        self.system_id = ""
        self.app_disabled = False
        self.batch = False
        self.enddate = ""
        self.status = ""
        self.id = ""
        self.startdate = ""
        self.app_description = ""
        self.notify = False
        self.resultfolderid = ""
        self.app_name = ""
