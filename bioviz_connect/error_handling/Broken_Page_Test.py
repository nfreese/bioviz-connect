import requests
from rest_framework.views import APIView
from bioviz_connect.error_handling.Custom_503_Exception_Handler import ServiceUnavailable
def get503Error():
    r = requests.get("http://localhost:3000/get503error")
    if(r.status_code == 503):
        raise ServiceUnavailable()
