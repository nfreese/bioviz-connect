# https://kc.cyverse.org/auth/realms/CyVerse/protocol/openid-connect/auth?client_id=bioviz&state=1a5e4abd-8dd7-4718-84ff-bc1612b122ee&redirect_uri=http%3A%2F%2Flocalhost%3A8000%2Foauth%3Fauth_callback%3D1&scope=openid&response_type=code
from .. import settings


def keycloakLogin():
    response = settings.AUTH_URL + "?" + "response_type=code&client_id=" + settings.CLIENT_ID + "&redirect_uri=" + settings.REDIRECT_URI
    response = settings.AUTH_URL + "?" + \
               "client_id=" + settings.CLIENT_ID + \
               "&response_type=code" \
               "&redirect_uri=" + settings.REDIRECT_URI +\
               "&state=1a5e4abd-8dd7-4718-84ff-bc1612b122ee" \
               "&auth_callback=1" \
               "&scope=openid";
    print(response)
    return response


