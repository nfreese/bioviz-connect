import requests
from ..terrain_model.RequestCreator import RequestCreator
import json

def uploadFileBy_Url(address, dest, redis_sessiondata):
    try:
        accesstoken = redis_sessiondata[0]
        request = RequestCreator.uploadFileByURL_request(address, dest)
        req_url = 'https://de.cyverse.org/terrain/secured/fileio/urlupload'
        r = requests.post(req_url, headers={'Accept': 'application/json',
                                        'Authorization': 'BEARER ' + accesstoken,
                                        'Content-type': 'application/json'},data=request)
        json_data = r.json()
        if 'error_code' in json_data:
            if (json_data['error_code'] == 'ERR_EXISTS'):
                raise FileExistsError()
            if (json_data['error_code'] == 'ERR_DOES_NOT_EXIST'):
                raise FileNotFoundError()
            if (json_data['error_code'] == 'ERR_TOO_MANY_PATHS'):
                raise BufferError()
        return json_data
    except FileExistsError as error:
        raise FileExistsError()
    except FileNotFoundError as error:
        raise FileNotFoundError()
    except BufferError as error:
        return BufferError()
    except Exception as error:
        return Exception()
