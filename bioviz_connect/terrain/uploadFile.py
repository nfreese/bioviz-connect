# upload file
import requests
from bioviz_connect.bioviz_connect import settings


def uploadFile(uploadFile, dest, accesstoken):

    req_url = 'https://de.cyverse.org/terrain/secured/fileio/upload?dest='+dest
    with open(uploadFile, 'rb') as f:
        r = requests.post(req_url, files={uploadFile: f},
                          headers={'Authorization': 'BEARER ' + accesstoken})
        print(r)
        print(r.text)


