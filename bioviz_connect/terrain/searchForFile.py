import requests
from .. import settings
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def searchForFile(exact, label, size, type, offset, sort_field, sort_order, accesstoken, username, home_path, community_path):
    try:
        #request_data = '{"query": {"all":[{"type": "label","args": {"exact": true, "label": "ML_Project"}},{"type": "owner","args": {"owner": "pawanbole"}}]},"size": 10,"from": 0,"sort": [{"field": "dateCreated","order": "descending"}]}'

        req_url = 'https://de.cyverse.org/terrain/secured/filesystem/search'
        if not exact:
            exact = False

        if sort_field == "NAME":
            sort_field = "label"
        elif sort_field ==  "LASTMODIFIED":
            sort_field = "dateModified"
        elif sort_field == "SIZE":
            sort_field = "fileSize"
        else:
            sort_field = "dateCreated"
        if sort_order=="ASC":
            sort_order = "ascending"
        else:
            sort_order="descending"
        request_Data = RequestCreator.create_searchFile_request(exact, label,  username, size, type, offset, sort_field, sort_order, home_path, community_path)
        r = requests.post(req_url, headers={'Authorization': 'BEARER ' + accesstoken,
                                        'Connection': 'keep-alive',
                                        'Content-Type': 'application/json'},
                                        data= request_Data)

        json_data = r.json()

        if 'error' in json_data:
            if 'no Elasticsearch' in json_data:
                raise Exception()

        if 'error_code' in json_data:
            if (json_data['error_code'] == 'ERR_NOT_AUTHORIZED'):
                raise PermissionError()

        response = ResponseParser.parse_searchResponse(json_data, type,  home_path, community_path, username)
        custom_encoder = CustomEncoder()
        res = custom_encoder.encode(response)
        return res
    except PermissionError as error:
        raise PermissionError()
    except Exception:
        return json_data['error']


