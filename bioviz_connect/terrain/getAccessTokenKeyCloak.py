import requests
from .. import settings


def getAccessTokenKeyCloak(code):
    r = requests.post(settings.ACCESS_TOKEN_URL,
                      headers={'Content-Type': 'application/x-www-form-urlencoded','Access-Control-Allow-Credentials' :'true'},
                      data={'grant_type': 'authorization_code',
                            'code': code,
                            'client_id': settings.CLIENT_ID,
                            'client_secret': settings.CLIENT_SECRET,
                            'redirect_uri': settings.REDIRECT_URI,
                            'scope': 'openid email profile'
                            })
    access_token = r.json()["access_token"]
    settings.TOKEN_EXPIRY = r.json()["expires_in"]
    return access_token

