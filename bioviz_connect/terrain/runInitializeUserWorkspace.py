# run coverage graph analysis
import requests
from .. import settings
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.CustomEncoder import CustomEncoder

def runInitializeUserWorkspace(accesstoken):
    try:
        req_url = 'https://de.cyverse.org/terrain/secured/bootstrap'
        r = requests.get(req_url, headers={'Authorization': 'BEARER ' + accesstoken,
                                        'Content-Type': 'application/json'},)

        json_data = r.json()
        if 'error_code' in json_data:
            if (json_data['error_code'] == 'ERR_NOT_AUTHORIZED'):
                raise PermissionError()

        custom_encoder = CustomEncoder()

        res = custom_encoder.encode(r.json())
        return res
    except PermissionError as error:
        raise PermissionError()


